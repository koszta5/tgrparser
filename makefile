SHELL := /bin/bash
PYTHONPATH=$PYTHONPATH:$(pwd)/src
AKTUALNI_UKOL=5
$(shell chmod 777 ./src/bash/buildHw.sh)
$(shell chmod 777 ./src/bash/clean.sh)
$(shell chmod 777 ./src/bash/deploy.sh)

build:
	./src/bash/buildHw.sh ${AKTUALNI_UKOL}
clean:
	./src/bash/clean.sh	
deploy:
	./src/bash/deploy.sh ${AKTUALNI_UKOL}
