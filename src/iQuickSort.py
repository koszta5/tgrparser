'''
Created on Mar 29, 2012

@author: Pavel Kostelnik
'''

class iQuickSort(object):
    '''
    I took basic code here -> http://hetland.org/coding/python/quicksort.html and adjusted it to fix my needs
    '''


    def __init__(self):
        pass
    
    def quicksort(self, listToSort, start, end):
        if start < end:                            # If there are two or more elements...
            split = self.partition(listToSort, start, end)    # ... partition the sublistToSort...
            self.quicksort(listToSort, start, split-1)        # ... and sort both halves.
            self.quicksort(listToSort, split+1, end)
        else:
            return 
        
    
    def partition(self, listToPartition, start, end):
        pivot = listToPartition[end]                          # Partition around the last value
        bottom = start-1                           # Start outside the area to be partitioned
        top = end                                  # Ditto
    
        done = 0
        while not done:                            # Until all elements are partitioned...
    
            while not done:                        # Until we find an out of place element...
                bottom = bottom+1                  # ... move the bottom up.
    
                if bottom == top:                  # If we hit the top...
                    done = 1                       # ... we are done.
                    break
    
                if listToPartition[bottom].nazev > pivot.nazev:           # Is the bottom out of place?
                    listToPartition[top] = listToPartition[bottom]       # Then put it at the top...
                    break                          # ... and start searching from the top.
    
            while not done:                        # Until we find an out of place element...
                top = top-1                        # ... move the top down.
                
                if top == bottom:                  # If we hit the bottom...
                    done = 1                       # ... we are done.
                    break
    
                if listToPartition[top].nazev < pivot.nazev:              # Is the top out of place?
                    listToPartition[bottom] = listToPartition[top]       # Then put it at the bottom...
                    break                          # ...and start searching from the bottom.
    
        listToPartition[top] = pivot                          # Put the pivot in its place.
        return top
    
        