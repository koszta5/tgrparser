#!/bin/bash
CISLO=$1
HW="hw${CISLO}"
SRC="$(pwd)/src"

cd "./src/${HW}"
for i in $(ls)
    do
        if [ $i = "__init__.py" ]
            then
            echo "nothing to be done for package initializator"
            else
            filename=${i%.*}
            touch $filename
            chmod 777 $filename
            echo "#!/bin/bash" >> $filename
            echo "export PYTHONPATH=\"\$PYTHONPATH:${SRC}\"" >> $filename
            echo "python ./src/${HW}/${filename}.py \$@" >> $filename
            mv $filename ../../
        fi
    done