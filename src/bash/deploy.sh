#!/bin/bash
CISLOUKOLU=$1
cd ..
DEPLOYNAME="tgrparser_deploy_hw$CISLOUKOLU"
ZIPNAME="$DEPLOYNAME.zip"
cp -r tgrparser $DEPLOYNAME
echo "copying and getting ready"
cd $DEPLOYNAME
rm -rf ./.git
echo "removed git nature"
cd ./src
rm -rf doc
echo "removed docs"
cd ../..
zip -vr $ZIPNAME $DEPLOYNAME
rm -rf $DEPLOYNAME
echo "ready to deploy (file --> tgrparser_deploy.zip) !!!"
