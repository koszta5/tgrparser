'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
import sys
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
  
        
    def main(self, argv):
        parser = Parser()
        try:
            letterFrom = argv[1]
            letterTo = argv[2]
            
        except IndexError, e:
            print "#!error={algo has to have 2 arguments(start and endnode)}"
            return
        parser.purpose = "self.graph.safestPath('"+letterFrom+"','"+letterTo+"')"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main(sys.argv)