'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
from edge import Edge
from iQuickSort import iQuickSort
from util import Util
import collections
from node import Node
from math import log10
class Graph(object):
    '''
    Class to represent graph. It mostly has complex function that work with its list of nodes
    '''


    def __init__(self):
        '''
        Contructor just initializes the listOfNodes that represent the graph object
        It also sets the graph as oriented by default (it is changed by parser class if there is any - in given string - we were told only either orineted and not oriented graphs will be given)
        '''
        self.nodeList =[]
        self.oriented = True
        
    def addNode(self, node):
        self.nodeList.append(node)
   
    def isNotMultiGraph(self, silent=False):
        """
        Function that finds out whether graph is not multigraph
        Algorhythm goes like this:
        1. get letter list for the whole graph
        2. iterate over nodes
        3. iterate over edges of the node
        4. add all letters of the node in list and compare it to list of all letters for the graph
        5. if there is a letter twice in letter list for the node, it is a multigraph
        """
        multi = False
        letterList = []
        for node in self.nodeList:
            letterList.append(node.nazev)
        for node in self.nodeList:
            nodeToNodes = []
            for edge in node.edgeList:
                nodeToNodes.append(edge.toNode.nazev)
            for letter in letterList:
                if (nodeToNodes.count(letter) > 1):
                    multi = True
        if (multi and not silent):
            print "#!isNotMultigraph=false"
        else:
            if (not multi and not silent):
                print "#!isNotMultigraph=true"
        return multi
    
    def isSimple(self, silent=False):
        """
        @param silent: if True, do not print anything just return value
        Algorhythm to determine if graph is simple or not:
        It goes like this:
        0. determine if graph is multigraph -> if so it is not simple therefore set simple = False
        1. iterate over nodes
        2, iterate over their edges
        3. create a letter list of all edges of the node
        4. find out if the node has a loop(edge to itself)
        5. if so set simple = False
        
        """
        simple = True
        if not (self.isNotMultiGraph(silent=True)):
            for node in self.nodeList:
                letterList = []
                for edge in node.edgeList:
                    letterList.append(edge.toNode.nazev)
                pocet = node.edgeListToLetters().count(node.nazev)
                if (pocet > 0):
                    simple = False
        else:
            simple = False
        if (simple and not silent):
            print "#!isSimple=true"
        else:
            if not silent:
                print "#!isSimple=false"
        return simple
        
    
    def hasLoop(self):
        """
        this detects loops in graph and adds them to a list
        Algorhythm goes like this:
        1. iterate over nodes
        2. iterate over edges
            3. create list of all nodes (letter type) to which node is connected
            4. find out if there is a letter with the same name as the node has in the list mentioned above
            5. if so, append the edge to loopList and set hasLoop =True
        """
        
        hasLoop = False
        loopList = []
        for node in self.nodeList:
            letterList = []
            for edge in node.edgeList:
                letterList.append(edge.toNode.nazev)
            pocet = node.edgeListToLetters().count(node.nazev)
            if (pocet > 0):
                loopList.append(node.findEdgeByNodeTo(node))
                hasLoop = True
        if (hasLoop):
            print "#!hasLoop=true"
            for edge in loopList:
                edge.printIsoEdge(loop=True)
        else:
            print "#!hasLoop=false"
    def isSymetric(self):
        """
        This finds out whether graph is symetric. Meaning if it has all edges both ways. It goes over the graph like this:
        1. iterate over nodes
        2. iterate over edges of the given node
        3. is the edge both ways?
            4. if not is the edge a loop?
                5. if not is the count of edges same from both ends
                    6. if not graph is not symetric
        """
        
        symetric = True
        for node in self.nodeList:
            for edge in node.edgeList:
                if not edge.isBothWays:
                    if not edge.toNode == node:
                        if not (len(edge.toNode.edgesByLetter(node.nazev)) == len(node.edgesByLetter(edge.toNode.nazev))):
                            symetric = False
        #make sure that the graph is oriented - fix
        if not self.oriented:
            print "#this graph is NOT oriented"
            symetric = False
            
        if (symetric):
            print "#!isSymetric=true"
            
        else:
            print "#!isSymetric=false"
    def findNode(self,pnode):
        """
        Get node of the graph - mostly just to find out if the node is present
        @param pnode: node to search for
        """
        searchedNode = None
        for node in self.nodeList:
            if node == pnode:
                searchedNode = node
        return searchedNode
    
    def findNodeByOrd(self, ord):
        for node in self.nodeList:
            try:
                if node.ord == ord:
                    return node
            except AttributeError,e:
                pass
    
    
    def subGraph(self, nodeList):
        """
        @param nodelist: expected list of strings 
        This function returns subgraph, it also damages the original graph structure (removes some edges from the graph)
        
        """
        def letterListToNodes(letterList):
            """
            Simple helped function to convert list of strings of nodes into list of nodes object
            """
            nodeList = []
            for letter in letterList:
                node = self.findNodeByName(letter)
                if node:
                    nodeList.append(node)
            return nodeList
        def stripEdgesWithUnknownNodes(graph):
            """
            @param graph: graph that is to be stripped
            Removes all the edges that have nodes that are not in the subgraph
            Algo goes like this:
            1. iterate over nodes and edges
            2. if there is an edge with ending point in node not in graph append edge to edgesToRemove
            3. iterate over nodes and edges again
            4. if the edge is in edgesToRemove append edge to toBeDeletedList
            5. go over toBeDeleted list and remove edges from the node
            """
            edgesToRemove = []
            for node in graph.nodeList:
                for edge in node.edgeList:
                    pocet = graph.nodeList.count(edge.toNode)
                    if (pocet == 0):
                        edgesToRemove.append(edge)
                edgesToRemove = list(set(edgesToRemove))
            for node in graph.nodeList:
                toBeDeletedList = []
                for edge in node.edgeList:
                    pocet = edgesToRemove.count(edge)
                    if (pocet > 0):
                        toBeDeletedList.append((edge))
                for edgee in toBeDeletedList:
                    node.edgeList.remove(edgee)
             
        nodeList = letterListToNodes(nodeList)
        newGraph = Graph()
        for node in nodeList:
            newGraph.addNode(node)
        stripEdgesWithUnknownNodes(newGraph)
            
        
        for node in newGraph.nodeList:
            node.printIsoNode()
        for node in newGraph.nodeList:
            for edge in node.edgeList:
                edge.printIsoEdge(node.nazev) 
        return newGraph 
                    
    def isComplete(self):
        """
        Function that determines if graph is complete or not. It solves whether it is simple first and then it goes on and determines algorhytm like this:
        1. iterate over nodes 
        2. get # of nodes in the graph
        3. deminish this number by 1 with each edge that is processed
        4. if allNodes number is > 1 there is an node that does not yet have edge and therefore the graph is not complete
        """
        if self.isSimple(silent=True):
            isComplete = True
            for node in self.nodeList:
                allNodes = len(self.nodeList)
                for edge in node.edgeList:
                    allNodes = allNodes - 1 
                if allNodes > 1:
                    isComplete = False
            if isComplete:
                print "#!isSimpleComplete=true"
            else: 
                print "#!isSimpleComplete=false"
        else:
            print "#!isSimpleComplete=none"
            
    def isRegular(self):
        """
        FInds out whether the graph is regular or not
        Alogorhytm goes like... go over nodes and append their degrees in a list
        Ask whether all the nodes have the same degree
        if so -> regular... 
        @return: string that indicates regularity
        
        """
        regular = "False"
        degreeField = []
        if not (self.isNotMultiGraph(silent=True)):
            for node in self.nodeList:
                degreeField.append(node.degree(self, silent=True))
            if (degreeField.count(degreeField[0]) == len(self.nodeList)):
                regular = "True"
        else:
            regular = "None"
        if (regular == "True"):
            print "#!isRegular=true"
            print "#!regularDegree="+str(degreeField[0])
        if (regular == "False"):
            print "#!isRegular=false"
        if (regular == "None"):
            print "#!isRegular=None"
        return regular
    def symetrize(self):
        """
        Symetrize the whole graph object
        """
        for node in self.nodeList:
            for edge in node.edgeList:
                edge.isBothWays=True
                edge.toNode.addEdge(Edge(toNode=node,isBothWays=True,hodnota=edge.hodnota, popis=edge.popis))
         
    
    def isBipartit(self):
        """
        Detect if the graph is bipartit or not. Basic idea of coloring nodes "stolen" from here:
        http://en.wikipedia.org/wiki/Bipartite_graph
        Has some inner funcitons to help the coloring.
        """
        def shiftIndex(index):
            if index == 1:
                return 0
            if index == 0:
                return 1
        def alreadyColored(node):
            if node.hodnota in ["Red", "Blue"]:
                return True
            else:
                return False
        def getColor(i):
            if i == 0:
                return "Red"
            if i == 1:
                return "Blue"
        def colorNodes(nodelist,i):
            while True:
                if not len(nodelist):
                    break
                node = nodelist.pop()
                if alreadyColored(node):
                    if (node.hodnota != getColor(i)):
                        self.isBipartitGraph = False
                        break
                else:
                    node.hodnota = getColor(i)
                    colorNodes(node.neighbors(self, smer="Out", silent = True), shiftIndex(i))
        
        if not (self.isNotMultiGraph(silent=True)):
            self.isBipartitGraph = True
            i = 0
            colors = ["Red", "Blue"]
            node = self.nodeList[0]
            node.popis = colors[i]
            i = shiftIndex(i)
            colorNodes(node.neighbors(self, smer="Out", silent = True), i)
            if self.isBipartitGraph:
                print "#!isBipartit=true"
            else:
                print "#!isBipartit=false"
        else:
            print "#!isBipartit=none"
        
        
    def nodeObejctExists(self, node):
        """
        Detects whether given node object in graph
        @param node: the node object to search for in graph
        """
        if (self.nodeList.count(node)):
            return True
        else:
            return False
    
    
    def numberOfNodes(self):
        return len(self.nodeList)
    
    
    def listNodes(self):
        """
        Function to list all the nodes in graph
        """
        print "### THIS IS NODE LIST ###"
        for node in self.nodeList:
            print "+-- node --+"
            print "+name: " + node.nazev 
            print "+value: " + str(node.hodnota) 
            print "+----------+"
    
    
    
    def findNodeByName(self, name):
        """
        Function to get node by its name
        @param name: name of a node to search for in the graph
        @return: node object with the given name that the function searched for
        """
        for node in self.nodeList:
            if (node.nazev == name):
                return node
     
    
    def findNodeByEdge(self, edge):
        """
        Returns a node that has the given edge object assigned
        @param edge: edge object to search for in the graph
        """
        searchedNode = None    
        for node in self.nodeList:
            for pedge in node.edgeList:
                if (pedge == edge):
                    searchedNode = node
        return searchedNode
    def containsNodeWithName(self, name):
        """
        Function to detect whether there is a node with a given name in the graph
        @param name: name to search for in the graph
        @return: True or False depending on whether the node with given name was found or not
        """ 
        contains = False
        for node in self.nodeList:
            if (node.nazev == name):
                contains = True
        return contains
    
    def nodesHaveConnection(self, node1, node2):
        """
        This function determines whether the two given nodes in parameters already have a connection and if so it returns the nodeFrom of this connection
        If in fact, there is no connection the function returns false
        @param node1: first node to be examined with node2
        @param node2: second node to be examined with node1
        @return: nodeFrom if connection found, False otherwise    
        
        """
        connection = False
        edgeList1 = node1.edgeList
        edgeList2 = node2.edgeList
        nodeFrom = None
        for edge1 in edgeList1:
            if (edge1.toNode == node2):
                connection = True
                nodeFrom = node1
        for edge2 in edgeList2:
            if (edge2.toNode == node1):
                connection = True
                nodeFrom = node2
        if (connection):
            return nodeFrom
        else:
            return connection    
    
    def sortListOfNodes(self, listToSort=[]):
        """
        Sorts list of nodes by name of the node
        @param listToSort: list to be sorted
        @return: sorted list of nodes
        """
        sorter = iQuickSort()
        print "#sorting list of nodes by letter using iQuicksort insatnce"
        sorter.quicksort(listToSort, 0, len(listToSort)-1)
        
        return listToSort
        
    

    def levelOrder(self, letter):
        """
        Levelorder pass through structure of graph
        @param letter: letter of the node to start from
        """
        nodeFifo = []
        out =[]
        def walk(node):
            localList = []
            for edge in node.edgeList:
                localList.append(edge.toNode)
            if (len(localList) > 1):    
                localList = self.sortListOfNodes(localList)
            for item in localList:
                if item not in out:
                    nodeFifo.append(item)
            nodeFifo.remove(node)
            if node not in out:
                out.append(node)
            if len(nodeFifo):
                walk(nodeFifo[0])
            else:
                print "#levelOrderFinished-->Output"
                
        startNode = self.findNodeByName(letter)
        nodeFifo.append(startNode)
        walk(startNode)
        for node in out:
            node.printIsoNode()
        return out

    
    def preOrder(self, letter, silent=False):
        """
        Preorder function with walk function called recursively
        @param letter: letter of the node to start from
        """
        out = []
        def walk(node):
            out.append(node)
            if node.degree(self, silent=True):
                childNodes = []
                for edge in node.edgeList:
                    if edge.toNode not in out:
                        childNodes.append(edge.toNode)
                childNodes = self.sortListOfNodes(childNodes)
                for childNode in childNodes:
                    walk(childNode)

        
        startNode = self.findNodeByName(letter)
        walk(startNode)
        if not silent:
            print "#preOrderFinished-->Output"
            for node in out:
                node.printIsoNode()
        return out
    
    def simpleCycleFinder(self):
        """
        Preorder function with walk function called recursively
        @param letter: letter of the node to start from
        """
        out = []
        self.cycleCounter = 0
        def walk(node):
            if node.hodnota=="mark":
                print "cycle found!!!"
                self.cycleCounter = self.cycleCounter +1
            else:
                node.hodnota = "mark"
            out.append(node)
            if node.degree(self, silent=True):
                childNodes = []
                for edge in node.edgeList:
                    if edge.toNode not in out:
                        childNodes.append(edge.toNode)
                childNodes = self.sortListOfNodes(childNodes)
                for childNode in childNodes:
                    walk(childNode)
        startNode = self.nodeList[0]
        walk(startNode)
        numberOfBothWays = 0
        for node in self.nodeList:
            for edge in node.edgeList:
                if edge.isBothWays:
                    numberOfBothWays = numberOfBothWays + 1
        numberOfBothWays = numberOfBothWays / 2
        return self.cycleCounter - numberOfBothWays
                    
        
        return out
    def inOrder(self, letter):
        out = []
        def walk(node):
            node.hodnota = "mark"
            if node.degree(self,smer="Out" , silent=True):
                childNodes = []
                for edge in node.edgeList:
                    if not edge.toNode.hodnota == "mark":
                        childNodes.append(edge.toNode)
                if (len(node.edgeList) > 1):
                    childNodes=self.sortListOfNodes(childNodes)
                try:
                    walk(childNodes[0])
                except IndexError,e:
                    print "#reached a leaf continuing recursively"
                if node not in out:
                    out.append(node)
                try:
                    walk(childNodes[1])
                except IndexError,e:
                    print "#can not walk right child - doesnt exist"
            else:
                if node not in out:
                    out.append(node)
                
        startNode = self.findNodeByName(letter)
        walk(startNode)
        for node in out:
            node.hodnota = None
            node.printIsoNode()
        return out            
    def postOrder(self, letter):
        out = []
        def walk(node):
            node.hodnota = "mark"
            if node.degree(self,smer="Out" , silent=True):
                childNodes = []
                for edge in node.edgeList:
                    if not edge.toNode.hodnota == "mark":
                        childNodes.append(edge.toNode)
                if (len(node.edgeList) > 1):
                    childNodes=self.sortListOfNodes(childNodes)
                try:
                    walk(childNodes[0])
                except IndexError,e:
                    print "#reached a leaf continuing recursively"
                try:
                    walk(childNodes[1])
                except IndexError,e:
                    print "#can not walk right child - doesnt exist"
                if node not in out:
                    out.append(node)
                
            else:
                if node not in out:
                    out.append(node)
                
        startNode = self.findNodeByName(letter)
        walk(startNode)
        for node in out:
            node.hodnota = None
            node.printIsoNode()
        return out
    
    def components(self):
        componentsList = []
        def removeAllComponentsWithNode(node, componentList=[]):
            for component in componentList:
                if node in component:
                    componentList.remove(component)
            return componentList
        
        for node in self.nodeList:
            component = self.preOrder(node.nazev, silent=True)
            nodeIsInAnyComponent = False
            for oldComponent in componentsList:
                if node in oldComponent:
                    nodeIsInAnyComponent = True
                    if (len(component) > len(oldComponent)):
                        componentsList.remove(oldComponent)
                        for oldNode in oldComponent:
                            componentsList = removeAllComponentsWithNode(oldNode, componentsList)
                        componentsList.append(component)
            if not nodeIsInAnyComponent:
                for subNode in component:
                    componentsList = removeAllComponentsWithNode(subNode, componentsList)
                componentsList.append(component)
        
        
        print "#!components="+str(len(componentsList))
        util = Util()
        componentsList = util.sortListOfLists(componentsList)
        for i in range(0,len(componentsList)):
            print "#!component_"+str(i)+"="+str(len(componentsList[i]))
            component = componentsList[i]
            for node in component:
                node.printIsoNode()
    
    def cycles(self, silent=False):
        """
        This is cycle detector
        @return: list of lists of cycles in the graph
        """
        nodeStack = []
        index = [0]
        nodeIndexes = {}
        nodeLowest = {}
        out = []


        def willNodeFinishCycle(node, nodeStack):
            if (node == nodeStack[0]):
                return True
            else:
                return False

        def walkCycle(node):
            nodeIndexes[node] = index[0]
            nodeLowest[node] = index[0]
            index[0] =+ 1
            nodeStack.append(node)
            children = node.neighbors(self, smer="Out", silent=True)
            for childNode in children:
                if (willNodeFinishCycle(childNode, nodeStack)):
                    # extend list
                    children.append(None)
                    # move everything one place back
                    children = collections.deque(children)
                    children.rotate(1)
                    # add the finishing node to head of the list
                    children[0] = childNode

            for childNode in children:

                if childNode not in nodeLowest:
                    # this node is a new one 
                    walkCycle(childNode)
                    nodeLowest[node] = min(nodeLowest[node], nodeLowest[childNode])
                else:
                    if (nodeStack[0] == childNode):
                        component = []
                        if len(nodeStack) > 1:
                            for mynode in nodeStack:
                                component.append(mynode)
                            out.append(component)
                    if childNode in nodeStack:
                        nodeLowest[node] = min(nodeLowest[node], nodeIndexes[childNode])

            if (nodeLowest[node] == nodeIndexes[node]):

                # dump the connected component from nodeStack
                component = []
                while True:
                    nextChild = nodeStack.pop()
                    component.append(nextChild)
                    if nextChild == node:
                        break
                if (len(component) > 1):
                    out.append(component)



        for i in range(0, len(self.nodeList)):
            nodeLowest = {}
            self.nodeList = collections.deque(self.nodeList)
            self.nodeList.rotate(1)
            for node in self.nodeList:
                if node not in nodeLowest:
                    walkCycle(node)
        util = Util()
        out = util.uniqueListOfLists(out)
        if not silent:
            print "#!cycles="+str(len(out))
            j = 0
            for cycle in out:
                print "#!cycle_"+str(j)
                j = j+ 1
                cycle = util.orderCycle(cycle)
                for i in range(0,len(cycle)):
                    cycle[i].printIsoNode()
                    if i == len(cycle)-1:
                        edge = cycle[i].findEdgeByNodeTo(cycle[0])
                    else:
                        edge = cycle[i].findEdgeByNodeTo(cycle[i+1])
                    edge.printIsoEdge(cycle[i])

        return out
   
    def jarnik(self):
        """
        this function is an implementation of Jarnik/Dijkstra algorhitm
        """ 
        outEdges = []
        outNodes = []
        def walk(nodeList):
            values = []
            avaliableEdges = []
            for node in nodeList:
                for edge in node.edgeList:
                    if edge not in outEdges:
                        avaliableEdges.append(edge)
                        values.append(edge.hodnota)
            try:
                shortestEdge = avaliableEdges[values.index(min(values))]
                execute = True
            except ValueError,e:
                execute = False
                #dumb assignment not to raise error
                shortestEdge = outEdges[0]
            while shortestEdge.toNode in outNodes and execute:
                try:
                    indexToRemove = values.index(min(values))
                    del values[indexToRemove]
                    del avaliableEdges[indexToRemove]
                    shortestEdge = avaliableEdges[values.index(min(values))]
                except ValueError,e:
                    execute = False
                    break
            
            outEdges.append(shortestEdge)
            outNodes.append(shortestEdge.toNode)
            if (set(outNodes) != set(self.nodeList)):
                walk(outNodes)
                
        outNodes.append(self.nodeList[0])
        walk(outNodes)
        for edge in outEdges:
            edge.printIsoEdge(self.findNodeByEdge(edge))
        value = 0
        for edge in outEdges:
            value = value + edge.hodnota
        print "#!value="+str(value)
        return outEdges
    
    def kruskal(self):
        util = Util()

        spanningTree = []
        listOfIsolatedTrees = []
        for node in self.nodeList:
            tree = []
            tree.append(node)
            listOfIsolatedTrees.append(tree)
        allEdges = []
        for node in self.nodeList:
            for edge in node.edgeList:
                allEdges.append(edge)
        allEdges = util.orderListOfEdgesBySize(allEdges)
        
        def treeConnectsToAnotherTreeViaEdge(tree1, tree2, edge):
            connects = False
            for fromNode in tree1:
                if edge in fromNode.edgeList:
                    if edge.toNode in tree2:
                        connects = True
            for fromNode in tree2:
                if edge in fromNode.edgeList:
                    if edge.toNode in tree1:
                        connects = True
            return connects
            
        def mergeTree(tree1, tree2):
            mergedTree = []
            for node in tree1:
                mergedTree.append(node)
            for node in tree2:
                mergedTree.append(node)
            return mergedTree
        
        def walk(allEdges, listOfIsolatedTrees):
                newList = list(listOfIsolatedTrees)
                newEdgeList = list(allEdges)
                for edge in allEdges:
                    for tree1 in listOfIsolatedTrees:
                        for tree2 in listOfIsolatedTrees:
                            if tree1 != tree2:
                                if treeConnectsToAnotherTreeViaEdge(tree1, tree2, edge):
                                    spanningTree.append(edge)
                                    merge = mergeTree(tree1, tree2)
                                    try:
                                        newList.remove(tree1)
                                        newList.remove(tree2)
                                        newList.append(merge)
                                        newEdgeList.remove(edge)
                                        opposite = edge.getOppositeEdge(self)
                                        newEdgeList.remove(opposite)
                                    except ValueError,e:
                                        return spanningTree
                                    walk(newEdgeList, newList)
            
        sptree = walk(allEdges, listOfIsolatedTrees)
        sptree = util.fixDuplicitsInSpanningTree(sptree)
        value = 0
        for edge in sptree:
            edge.printIsoEdge(self.findNodeByEdge(edge))
            value = value + edge.hodnota
        print "#!value="+str(value)
                
        
    def boruvka(self):
        def mergeTree(tree1, tree2):
            mergedTree = []
            for node in tree1:
                mergedTree.append(node)
            for node in tree2:
                mergedTree.append(node)
            return mergedTree
        def treeConnectsToAnotherTreeViaEdge(tree1, tree2, edge):
            connects = False
            for fromNode in tree1:
                if edge in fromNode.edgeList:
                    if edge.toNode in tree2:
                        connects = True
            for fromNode in tree2:
                if edge in fromNode.edgeList:
                    if edge.toNode in tree1:
                        connects = True
            return connects
        
        util = Util()
        listOfIsolatedTrees = []
        for node in self.nodeList:
            tree = []
            tree.append(node)
            listOfIsolatedTrees.append(tree)
        setE = []
        while (len(listOfIsolatedTrees)> 1):
            otherTrees = list(listOfIsolatedTrees)
            for tree in listOfIsolatedTrees:
                setS = []
                otherTrees.remove(tree)
                avaliableEdges = []
                for node in tree:
                    for edge in node.edgeList:
                        connectsToAnother = False
                        for tree2 in otherTrees:
                            if treeConnectsToAnotherTreeViaEdge(tree, tree2, edge):
                                connectsToAnother = True
                        if connectsToAnother:
                            avaliableEdges.append(edge)
                    avaliableEdges = util.orderListOfEdgesBySize(avaliableEdges)
                    setS.append(avaliableEdges[0])
                setS = util.orderListOfEdgesBySize(setS)
                # zrus trees a merge trees
                treeFrom = util.findTreeWithEdgeFrom(setS[0], listOfIsolatedTrees)
                treeTo = util.findTreeWithEdgeTo(setS[0], listOfIsolatedTrees)
                newTree = mergeTree(treeFrom, treeTo)
                listOfIsolatedTrees.remove(treeFrom)
                listOfIsolatedTrees.remove(treeTo)
                listOfIsolatedTrees.append(newTree)
                setE.append(setS[0])
                break
        value = 0
        for edge in setE:
            edge.printIsoEdge(self.findNodeByEdge(edge))
            value = value + edge.hodnota
        print "#!value="+str(value)
        
    
    def stripEdgesToUknownNodes(self):
        for node in self.nodeList:
            print "i" + str(node.nazev)
            for edge in node.edgeList:
                if edge.toNode not in self.nodeList:
                    node.edgeList.remove(edge)
     
    def cloneGraphBySelection(self, letterList):
        g = Graph()
        for node in self.nodeList:
            if node.nazev in letterList:
                newNode = Node(nazev=node.nazev)
                newNode.nazev = node.nazev
                newNode.hodnota = node.hodnota
                g.nodeList.append(newNode)
        
        return g
        
    def cloneEdgeBySelection(self, edge, originalGraph):
        newEdge = Edge()
        newEdge.hodnota = edge.hodnota
        newEdge.popis = edge.popis
        newEdge.isBothWays = edge.isBothWays
        toNodeNazev = edge.toNode.nazev
        toNode = self.findNodeByName(toNodeNazev)
        newEdge.toNode = toNode
        fromNodeNazev = originalGraph.findNodeByEdge(edge).nazev
        self.findNodeByName(fromNodeNazev).edgeList.append(newEdge)
                                                       
    def dumpGraphToFile(self, name):
        f = open(name, "w+")
        for node in self.nodeList:
            nodeString = node.printIsoNode(silent=True)
            f.write(nodeString + "\n")
        for node in self.nodeList:
            for edge in node.edgeList:
                edgeString = edge.printIsoEdge(node, silent=True)
                f.write(edgeString+ "\n")
        f.close()    
            
    def oneWayOnly(self):
        """
        Returns an oriented graph that is created from not oriented one
        """    
        for node in self.nodeList:
            for edge in node.edgeList:
                edge.toNode.edgeList.remove(edge.toNode.findEdgeByNodeTo(node))
          
    def BellmanFordShortestPath(self, letterFrom, letterTo, silent = False):
        # prerequirements
        startNode = self.findNodeByName(letterFrom)
        endNode = self.findNodeByName(letterTo)
        
        def initAllNodes(letterFrom):
            for node in self.nodeList:
                if node.nazev == letterFrom:
                    node.hodnota = 0
                    node.pocet = 0 
                else:
                    node.hodnota = 99999
                    node.pocet = 0
        def negativeCycleCheck():
            for node in self.nodeList:
                for edge in node.edgeList:
                    if edge.toNode.hodnota > node.hodnota+edge.hodnota:
                        print "#CANNOT LOOK FOR SHORTEST PATH --> negative cycle detected"
                        return True
        # algo itself starts here
        initAllNodes(letterFrom)
        for i in range(0, len(self.nodeList)-1):
            step = 0
            for node in self.nodeList:
                changedNodes = []
                for edge in node.edgeList:
                    if edge.toNode.hodnota > node.hodnota+edge.hodnota:
                        edge.toNode.hodnota = node.hodnota + edge.hodnota
                        edge.toNode.prev = node
                        if edge.toNode.pocet:
                            edge.toNode.pocet = node.pocet +1
                        else:
                            edge.toNode.pocet = 1
                        changedNodes.append(edge.toNode.nazev)
                if not silent:
                    try:
                        print "#!step "+str(step)+" " +str(node.nazev)+"=("+str(node.pocet)+","+node.prev.nazev+","+str(node.hodnota)+") "+"," .join(changedNodes)
                    except AttributeError,e:
                        print "#!step "+str(step)+" " +str(node.nazev)+"=("+str(node.pocet)+",None,"+str(node.hodnota)+") "+"," .join(changedNodes)
                step += 1
        if not silent and not negativeCycleCheck():
            util = Util()               
            util.findWayTo(startNode, endNode)
        
    def dijkstraShortestPath(self, startLetter, endLetter):
        startNode = self.findNodeByName(startLetter)
        endNode = self.findNodeByName(endLetter)
        def initAllNodes():
            for node in self.nodeList:
                if node == startNode:
                    node.prev = None
                    node.hodnota = 0
                    node.perm = False
                else:
                    node.prev = None
                    node.hodnota = 99999
                    node.perm = False

        initAllNodes()
        # algo start 
        q = list(self.nodeList)
        step = 0
        print "#!step " + str(step)+ " " + startNode.nazev + " (None,"+str(startNode.hodnota)+")"
        step +=1
        while q:
            distanaces = []
            for node in q:
                distanaces.append(node.hodnota)
            indexU = distanaces.index(min(distanaces))
            u = q[indexU]
            if not u:
                print "not reachable"
                break
            q.remove(u)
            for edge in u.edgeList:
                alt = u.hodnota + edge.hodnota
                if alt < edge.toNode.hodnota:
                    edge.toNode.hodnota = alt
                    edge.toNode.prev = u
                    print "#!step " + str(step)+ " " + edge.toNode.nazev + " ("+str(edge.toNode.prev.nazev)+","+str(edge.toNode.hodnota)+")"
            step +=1
        util = Util()
        util.findWayTo(startNode, endNode)
        
    def findLongestPath(self, letterFrom, letterTo):
        startNode = self.findNodeByName(letterFrom)
        endNode = self.findNodeByName(letterTo)
        
        def findWayTo(startNode, endNode):
            currNode = endNode
            backtrackPath = []
            backtrackPath.append(currNode)
            
            pathExists = True
            while currNode != startNode:
                try:
                    backtrackPath.append(currNode.prev.findEdgeByNodeTo(currNode))
                    backtrackPath.append(currNode.prev)
                    currNode = currNode.prev
                except AttributeError,e:
                    print "#!pathNotExists"
                    pathExists = False
                    break
            
            backtrackPath.reverse()

            if (pathExists):
                length = 0   
                for i, obj in enumerate((backtrackPath)):
                    if isinstance(obj, Node):
                        obj.printIsoNode()
                    if isinstance(obj, Edge):
                        obj.hodnota = obj.origValue
                        obj.printIsoEdge(backtrackPath[i-1])
                        length = length + obj.hodnota
                print "#!value="+str(length)
        
        for node in self.nodeList:
            for edge in node.edgeList:
                edge.origValue = edge.hodnota
                edge.hodnota = -1 * edge.hodnota
        

        
        
        self.BellmanFordShortestPath(letterFrom, letterTo, silent=True)
        findWayTo(startNode, endNode)
                
    def floydWarshall(self, letterFrom, letterTo):
        # NOT FINISHED AND NOT WORKING --> unable to eliminate negative cycles
        def listMatrix(matrixOfTuples, step):
            print "#!step "+ str(step)+ "=",
            for i in range(1, len(self.nodeList)+1):
                for j in range(1, len(self.nodeList)+1):
                    print matrixOfTuples[i,j],
                print ";",
            print ""

       

        startNode = self.findNodeByName(letterFrom)
        endNode = self.findNodeByName(letterTo)
        weightMatrix = {}
        # sort all nodes and assign them an ordinal number
        self.sortListOfNodes(self.nodeList)
        for i,node in enumerate(self.nodeList):
            node.ord = i+1
        for node in self.nodeList:
            connectedNodes = []
            for edge in node.edgeList:
                weightMatrix[node.ord, edge.toNode.ord] = edge.hodnota
                connectedNodes.append(edge.toNode)
            # add maximum values for nodes that are not connected
            for notConnectedNode in self.nodeList:
                    if notConnectedNode not in connectedNodes:
                        if notConnectedNode == node:
                            weightMatrix[node.ord, node.ord] = 0
                        else:
                            weightMatrix[node.ord, notConnectedNode.ord] = 99999
        result = {0: weightMatrix}

       
        # initialize backtrack path
        util = Util()
        backtrackPath = util.make_matrix(len(self.nodeList)+1, len(self.nodeList)+1)
        listMatrix(result[0], step=0)
        for step in range(1, len(self.nodeList)+1):
            result[step] = {}
            for i in range(1, len(self.nodeList)+1):
                for j in range(1, len(self.nodeList)+1):
                    result[step][i,j] = min(result[step-1][i,j], result[step-1][i,step] + result[step-1][step,j])
                    if result[step-1][i,j] > result[step-1][i,step] + result[step-1][step,j]:
                        backtrackPath[i][j] = step    
            listMatrix(result[step], step=step)
        final = result[len(self.nodeList)]
        ordFrom = startNode.ord
        ordTo = endNode.ord
        length = 0
        for pair in final:
            if pair[0] == ordFrom and pair[1] == ordTo:
                length = final[pair]
        print "#!value="+str(length)
        foundPath = []
        def getBacktrackPath(i, j):
            pair = (i,j)
            if final[pair] == 99999:
                print "#no path exists"
            inter = backtrackPath[i][j]
            if not inter:
                print "#direct from "+str(self.findNodeByOrd(i).nazev) + " to "+str(self.findNodeByOrd(j).nazev)
                foundPath.append(self.findNodeByOrd(i))
                foundPath.append(self.findNodeByOrd(j))
                return ""
            else:
                return getBacktrackPath(i, inter) + str(inter) + getBacktrackPath(inter, j) 
        
        getBacktrackPath(startNode.ord, endNode.ord)
        foundPath.append(self.findNodeByOrd(endNode.ord))
        for i, node in enumerate(foundPath):
            if i % 2 ==0:
                node.printIsoNode()
                try:
                    edge = node.findEdgeByNodeTo(foundPath[i+1])
                except IndexError,e:
                    break
                edge.printIsoEdge(node)
        
    def safestPath(self, letterFrom, letterTo):
        
        startNode = self.findNodeByName(letterFrom)
        endNode = self.findNodeByName(letterTo)
        
        def findWayTo(startNode, endNode):
            currNode = endNode
            backtrackPath = []
            backtrackPath.append(currNode)
            
            pathExists = True
            while currNode != startNode:
                try:
                    backtrackPath.append(currNode.prev.findEdgeByNodeTo(currNode))
                    backtrackPath.append(currNode.prev)
                    currNode = currNode.prev
                except AttributeError,e:
                    print "#!pathNotExists"
                    pathExists = False
                    break
            
            backtrackPath.reverse()

            if (pathExists):
                length = 1   
                for i, obj in enumerate((backtrackPath)):
                    if isinstance(obj, Node):
                        obj.printIsoNode()
                    if isinstance(obj, Edge):
                        obj.hodnota = obj.origValue
                        obj.printIsoEdge(backtrackPath[i-1])
                        length = length * obj.hodnota
                length = length*100
                length = int(length) 
                print "#!value="+str(length) + "%"
        # change edges values...
        for node in self.nodeList:
            for edge in node.edgeList:
                edge.origValue = edge.hodnota
                edge.hodnota = -1*log10(edge.hodnota)
        self.BellmanFordShortestPath(letterFrom, letterTo, silent=True)
        findWayTo(startNode, endNode)
    
    
    def widestPath(self, letterFrom, letterTo):
        startNode = self.findNodeByName(letterFrom)
        endNode = self.findNodeByName(letterTo)
            
        def walk(node):
            node.perm = True
            values = []
            available = []
            for edge in node.edgeList:
                if not edge.toNode.perm:
                    values.append(edge.hodnota)
                    available.append(edge.toNode)
                if edge.toNode.hodnota < edge.hodnota and not edge.toNode.perm:
                    edge.toNode.hodnota = edge.hodnota
                    edge.toNode.prev = node
            try:
                maxIndex = values.index(max(values))
                walk(available[maxIndex])
            except ValueError,e:
                try:
                    walk(node.prev)
                except AttributeError,e:
                    return
        
        def findWayTo(startNode, endNode):
            currNode = endNode
            backtrackPath = []
            backtrackPath.append(currNode)
            
            pathExists = True
            while currNode != startNode:
                try:
                    backtrackPath.append(currNode.prev.findEdgeByNodeTo(currNode))
                    backtrackPath.append(currNode.prev)
                    currNode = currNode.prev
                except AttributeError,e:
                    print "#!pathNotExists"
                    pathExists = False
                    break
            
            backtrackPath.reverse()

            if (pathExists):
                lengthList = []   
                for i, obj in enumerate((backtrackPath)):
                    if isinstance(obj, Node):
                        obj.printIsoNode()
                    if isinstance(obj, Edge):
                        obj.printIsoEdge(backtrackPath[i-1])
                        lengthList.append(obj.hodnota)
                print "#!value="+str(min(lengthList))
        
        # algo start
        # init list
        for node in self.nodeList:
            node.prev = None
            node.hodnota = 0
            node.perm = False
        
        walk(startNode)        
        findWayTo(startNode, endNode)
        
    def isNetwork(self, silent=False):
        # check if there is source and check that it has only out edges
        network = True
        source = self.findNodeByName("S") 
        if not (source):
            network = False
        else:
            if (source.surrounding(self, smer="In", silent=True)):
                network = False
        stock = self.findNodeByName("T")
        if not (stock):
            network = False
        else:
            if (stock.surrounding(self,smer="Out", silent=True)):
                network = False
            
        # chekc if all the edges are + 
        for node in self.nodeList:
            for edge in node.edgeList:
                try:
                    if int(edge.hodnota) < 0 and not edge.isBothWays:
                        network = False
                except TypeError,e:
                    network = False
                    pass
        
        if network: 
            if not silent:
                print "#!isNetwork=true"
        else:
            print "#!isNetwork=false"
            
        return network
    
    def edmonds(self, source, stock, printType, silent=False):
        if not self.isNetwork(silent=True):
            return
        util = Util()
        capacity_matrix = util.create_capacity_matrix(self)
        residual_matrix = util.make_matrix(len(capacity_matrix), len(capacity_matrix))
        util.ordinalize_list_of_nodes(self)
        util.backup_edges_value(self)
        source = self.findNodeByName(source).ord
        stock = self.findNodeByName(stock).ord
        step = 0 
        allEdges = []
        
        while True:
            path = self.breadthFirstSearch(capacity_matrix, residual_matrix, source, stock)
            if not path:
                break
            minCandidates = []
            for fromNode, toNode in path:
                minCandidates.append(capacity_matrix[fromNode][toNode] - residual_matrix[fromNode][toNode])
            flow = min(minCandidates)
            stepPath, stepEdges = util.tuple_ord_path_to_node_list(self, path)
            allEdges.extend(stepEdges)
            print "#!step "+str(step)+"="+str(flow)+": "+stepPath
            step += 1
            for fromNode,toNode in path:
                # fix values in residual path
                residual_matrix[fromNode][toNode] = residual_matrix[fromNode][toNode] + flow
                residual_matrix[toNode][fromNode] = residual_matrix[toNode][fromNode] - flow
                # find coresponding edges and update their value
                # backup orig value (to compare whether it has some reesidue
                try:
                    self.findNodeByOrd(fromNode).findEdgeByNodeTo(self.findNodeByOrd(toNode)).hodnota = residual_matrix[fromNode][toNode]
                except AttributeError,e:
                    # for values against the flow
                    pass
                    
        maxFlow = 0
        if not silent:
            for i in range(0, len(capacity_matrix)):
                maxFlow = maxFlow + residual_matrix[source][i]
            print "#!value="+str(maxFlow)
            if printType == "FLOW":
                self.flowPrint(allEdges)
            if printType == "RESERVE":
                self.reservePrint(allEdges)
        return maxFlow
    
    def flowPrint(self, allEdges):
        # print all nodes
        for node in self.nodeList:
            node.printIsoNode()
        
        # only unique edges
        allEdges = list(set(allEdges))
        for edge in allEdges:
            # do not print oposite edges - they are saved as nones
            if edge:
                # print edge only if its capacity is fully used
                if edge.orig_hodnota==edge.hodnota:
                    edge.printIsoEdge(self.findNodeByEdge(edge))                    

    def reservePrint(self, allEdges):
        for node in self.nodeList:
            node.printIsoNode()
        
        # only unique edges
        allEdges =list(set(allEdges))
        for node in self.nodeList:
            for edge in node.edgeList:
                if edge in allEdges:
                    if (edge.hodnota != edge.orig_hodnota):
                        edge.printIsoEdge(self.findNodeByEdge(edge))
                else:
                    edge.printIsoEdge(self.findNodeByEdge(edge))
            
    def breadthFirstSearch(self, capacity, residual, source, stock):
        queue = [source]                 
        path = {source: []}
        while queue:
            fromNode = queue.pop(0)
            for toNode in range(0,len(capacity)):
                if capacity[fromNode][toNode] - residual[fromNode][toNode] > 0 and not toNode in path:
                    path[toNode] = path[fromNode] + [(fromNode, toNode)]
                    if toNode == stock:
                        return path[toNode]
                    queue.append(toNode)
        return None
    

    def goldberg(self, source, stock, printType):
        util = Util()
        util.ordinalize_list_of_nodes(self)
        util.backup_edges_value(self)
        source = self.findNodeByName(source).ord
        stock = self.findNodeByName(stock).ord
        
        capacity_matrix = util.create_capacity_matrix(self)
        residual_matrix = util.make_matrix(len(capacity_matrix), len(capacity_matrix))
        self.step = 0
        heigth = []
        overValue = []
        tried = []
        for i in range(0, len(capacity_matrix)):
            heigth.append(0)
            overValue.append(0)
            tried.append(0)
        
        sequence = []
        for i in range(0,len(capacity_matrix)):
            if i != source and i != stock:
                sequence.append(i)
        
        def wave(fromNode, toNode):
            minValue = min(overValue[fromNode], capacity_matrix[fromNode][toNode] - residual_matrix[fromNode][toNode])
            residual_matrix[fromNode][toNode] = residual_matrix[fromNode][toNode] + minValue
            residual_matrix[toNode][fromNode] = residual_matrix[toNode][fromNode] - minValue
            overValue[fromNode] = overValue[fromNode] - minValue
            overValue[toNode] = overValue[toNode] + minValue
    
        def liftNode(fromNode):
            min_heigth = heigth[fromNode]
            for toNode in range(0,len(capacity_matrix)):
                if capacity_matrix[fromNode][toNode] - residual_matrix[fromNode][toNode] > 0:
                    min_heigth = min(min_heigth, heigth[toNode])
                    heigth[fromNode] = min_heigth + 1
                    print "#!step "+str(self.step)+"="+self.findNodeByOrd(fromNode).nazev+":"+str(heigth[fromNode])
                    self.step = self.step + 1  
    
        def emptyAll(fromNode):
            while overValue[fromNode] > 0:
                if tried[fromNode] < len(capacity_matrix): 
                    toNode = tried[fromNode]
                    if capacity_matrix[fromNode][toNode] - residual_matrix[fromNode][toNode] > 0 and heigth[fromNode] > heigth[toNode]:
                        wave(fromNode, toNode)
                    else:
                        tried[fromNode] += 1
                else: 
                    liftNode(fromNode)
                    tried[fromNode] = 0
    
        heigth[source] = len(capacity_matrix)  
        overValue[source] = 99999
        for toNode in range(0, len(capacity_matrix)):
            wave(source, toNode)
    
        toGo = 0
        while toGo < len(sequence):
            fromNode = sequence[toGo]
            old_heigth = heigth[fromNode]
            emptyAll(fromNode)
            if heigth[fromNode] > old_heigth:
                sequence.insert(0, sequence.pop(toGo)) 
                toGo = 0 
            toGo += 1
        totalFlow = 0
        for i in range(0, len(capacity_matrix)):
            totalFlow = totalFlow + residual_matrix[source][i]
        
        #util.debug_ord_matrices(residual_matrix, capacity_matrix, self)
        util.compute_new_edges_value_from_matrix(self, residual_matrix, capacity_matrix)
        allEdges = []
        for node in self.nodeList:
            for edge in node.edgeList:
                allEdges.append(edge)
        print "#!value="+str(totalFlow)
        if printType == "FLOW":
            self.flowPrint(allEdges)
        if printType == "RESERVE":
            self.reservePrint(allEdges)
         
        return totalFlow
           
    def listEdges(self):
        """
        Function to list all the edges of the graph object
        It doesnt list both-way edges twice (it relies on printing isBothWays: True. So e.g. edge A - B will be listed as A - B only, not also B - A eventhough it is also implemented
        isBothWays is only a convinience property to simplify dealing with edges.
        """
        def printEdge(edge,node):
            """
            Inner function to print edge information
            @param edge: edge object to get the info from
            @param node: node from which the edge starts
            """
            print "+-- edge --+"
            print "+from node:" + node.nazev
            print "+to node:" + edge.toNode.nazev
            print "+isBothWays:" +str(edge.isBothWays)
            print "+value:" + str(edge.hodnota)
            print "+desc:" +str(edge.popis)
            print "+----------+"
        bothWaysFromNodes =[]
        bothWaysToNodes = []
        print "### THIS IS EDGE LIST ###"
        for node in self.nodeList:
            for edge in node.edgeList:
                if edge.isBothWays:
                    if not (bothWaysFromNodes.count(node) and bothWaysToNodes.count(edge.toNode) or (bothWaysToNodes.count(node) and bothWaysFromNodes.count(edge.toNode))):
                            printEdge(edge, node)
                            bothWaysFromNodes.append(node)
                            bothWaysToNodes.append(edge.toNode)
                else:
                    printEdge(edge, node)
                    
