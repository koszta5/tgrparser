'''
Created on Feb 16, 2012

@author: kosta
'''
import sys,re
from graphClass import Graph
from node import Node
from edge import Edge
class Parser(object):
    def __init__(self, purpose = None):
        self.graph =  Graph()
        self.purpose = purpose
    
    
    def run(self):
        """
        This funciton reads from stdIN and parses the input. Once there is no input it runs the purpose function for which it was created
        """
        while True:
            line = sys.stdin.readline()
            if line:
                self.parseLine(line)
            else:
                eval(self.purpose)
                sys.exit()
    
    def normalize(self,line):
        """
        This is the first function called before anything else - it trims the whitespace from cmd input
        @param line: raw line of command line input
        @return: trimmed line without trailing beginning, in-middle whitespace. It also eliminates system-specific EOL chars
        """
        line = re.sub('\t+', " ", line)
        line = line.rstrip('\r\n')
        return re.sub(' +', ' ', line)
    

    
                    
    def parseLine(self,line):
        """
        accoring to regex this function parses the line to desired object
        @param line: trimmed line to be parsed - it is detected whether it is node, edge or comment
        """
        line = self.normalize(line)
        if (re.match(r'^u (\w+)( +(-?\d+))?$', line)):
            self.parseToNode(line)
        else:
            if (re.match(r'^h (\w+) +(<|-|>) +(\w+)( +(-?\d+(\.\d+)?))?( +:(.+))?$', line)):
                self.parseToEdge(line)
            else:
                if (re.match(r'^#.*', line)):
                    self.parseComment(line)
                else:
                    print "#!error=wrongInput"
    
            
    def parseToNode(self, line):
        """
        It has been determined that the line is a new node. It detects an error being raised if node doenst hvae value. 
        It creates the new node object
        @param line: parsed, trimmed line to create the node from
    
        """ 
        newNode = line.split(" ")
        try:
            self.graph.addNode(Node(newNode[1], newNode[2]))
        except IndexError, e:
            self.graph.addNode(Node(newNode[1]))
            
    
    
    def parseToEdge(self, line):
        """
        It collects all the information it needs for edge creation (also prepares the name if present). Then according to way variable (what way the edge goes) it finds the desired nodes and calls createEdge()
        If one of the nodes from the edge are not present it raises an error
        @param line: it is the line that is to be parsed to edge 
        """
        newEdge = line.split(" ")
        node1 = newEdge[1]
        node2 = newEdge[3]
        way= newEdge[2]
        value = None
        name = None
        try:
            value = newEdge[4]
            if re.match(r'.*\..*', value):
                value = float(value)
            else:
                value = int(value)
        except IndexError, e:
            value = None
        try:
            name = newEdge[5]
            name = re.sub(":", "", name)
        except IndexError, e:
            name = None
        if (self.graph.containsNodeWithName(node1) and self.graph.containsNodeWithName(node2)):
            nodeFrom = None
            nodeTo = None
            if (way == ">"):
                nodeFrom = self.graph.findNodeByName(node1)
                nodeTo = self.graph.findNodeByName(node2)
                isBothWays = False
                self.createEdge(nodeFrom, nodeTo, isBothWays, value, name)
            else: 
                if (way == "<"):
                    nodeFrom = self.graph.findNodeByName(node2)
                    nodeTo = self.graph.findNodeByName(node1)
                    isBothWays = False
                    self.createEdge(nodeFrom, nodeTo, isBothWays, value, name)
                else:
                    if (way == "-"):
                        self.graph.oriented = False
                        nodeFrom = self.graph.findNodeByName(node1)
                        nodeTo = self.graph.findNodeByName(node2)
                        isBothWays = True
                        # detect if it is not a loop
                        if not (node1 == node2):
                            # if not - then create 2 edges
                            self.createEdge(nodeFrom, nodeTo, isBothWays, value, name)
                            self.createEdge(nodeTo, nodeFrom, isBothWays, value, name)
                        else:
                            # it is a loop create only one edge for the loop
                            self.createEdge(nodeFrom, nodeTo, isBothWays, value, name)
                    else:
                        print "#!error=wrongWayOfEdge"
        else:
            print "#!error=unknownNode"
        
         
   
    def createEdge(self,nodeFrom, nodeTo, isBothWays, hodnota, popis):
        """
        Function for edge object creation - extracted out of the main parsing algorhytm
        @param nodeFrom: node from which the edge goes
        @param nodeTo: node to which the edge goes
        @param isBothWays: a simple convinience indicator if the edge goes both ways
        @param hodnota: value of the edge
        @param popis: description of the edge
        """ 
        
        connectionAlradyExists = self.graph.nodesHaveConnection(nodeFrom, nodeTo)
        # This is very debatable piece of code -> imagine:
        #u A
        #u B
        #h A > B 5
        #h B > A 6
        # is it bothWays? -> I would say so but in order to get edges print nicely (the way they were ment to < > , I commented out these lines for now
        """if (connectionAlradyExists and connectionAlradyExists != nodeFrom):
            isBothWays= True
            connectionAlradyExists.findEdgeByNodeTo(nodeFrom).isBothWays= True"""
        edge = Edge(nodeTo, isBothWays, hodnota, popis)
        nodeFrom.addEdge(edge)
        
    def parseComment(self, line):
        pass

    def debug(self, text):
        print "DEBUG =="+ str(text)
    