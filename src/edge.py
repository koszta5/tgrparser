'''
Created on Feb 16, 2012
this is commited from local
@author: Pavel Kostelnik
'''

class Edge(object):
    '''
    This class represents the edge in the graph. It only exists as Agregation of Node object. Therefore it doesnt hold any reference to its owner object. Only to node it goes to.
    isBothWays

    '''


    def __init__(self, toNode=None, isBothWays=False, hodnota=None, popis=None):
        '''
        Basic contructor that is parametric and sets all the edge properties
        @param toNode: the node edge goes to (node object)
        @param isBothWays: convinience property to indicate both way relationship (eventhough it is still implemneted on both ends)
        @param hodnota: value of the edge
        @param popis: description of the edge
        '''
        self.toNode= toNode
        self.isBothWays= isBothWays
        self.hodnota = hodnota
        self.popis = popis
        
    
    def printIsoEdge(self, fromNode=None, loop=False, silent=False):
        if loop:
            fromNode = self.toNode.nazev
        else:
            fromNode = fromNode.nazev
        string = "h " + fromNode + " "
        if self.isBothWays:
            string += "- "
        else:
            string += "> "
        string += self.toNode.nazev + " "
        try:
            if self.hodnota:
                string += str(self.hodnota) + " "
        except AttributeError,e:
            pass
        if self.popis:
            string += ":" + self.popis
        if not silent:
            print string
        return string
    
    def getOppositeEdge(self, graph):
        myToNode = self.toNode
        for node in graph.nodeList:
            if node == myToNode:
                for edge in node.edgeList:
                    if edge.toNode == graph.findNodeByEdge(self):
                        return edge