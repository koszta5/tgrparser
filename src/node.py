'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
class Node(object):
    '''
    this class represents the node object of the graph. Each node contains a list of edges that start from the Node
    '''


    def __init__(self, nazev, hodnota=None):
        '''
        Node object is created by setting its basic values in the constructor
        '''
        self.nazev = nazev
        self.hodnota = hodnota
        self.edgeList = []
        
    def addEdge(self, edge):
        self.edgeList.append(edge)
    
    def numberOfEdges(self):
        return len(self.edgeList)
    

    def findEdgeByNodeTo(self, node):
        """
        This function returns searched edge that the given node is end of. This means returnedEdge->givenNodeParam
        @param node: node with which the edge is supposed to end
        @return: the edge that fulfill the requirements stated above 
        """
        searchedEdge = None
        for edge in self.edgeList:
            if edge.toNode == node:
                searchedEdge = edge
        return searchedEdge        
    def edgeListToLetters(self):
        """
        This returns a list with letters of all the nodes edges of this node goes to
        """
        letterList = []
        for edge in self.edgeList:
            letterList.append(edge.toNode.nazev)
        return letterList
    
    def edgesByLetter(self, letter):
        foundlist = []
        for edge in self.edgeList:
            if edge.toNode.nazev == letter:
                foundlist.append(edge)
        return foundlist
    
    def neighbors(self, graph=None, smer="Both", silent = False):
        """
        This function returns all the nodes that are neigbouring with this node
        @param graph: graph object the node is part of
        @param smer: way which to search for (in for nodes that go > self, out for nodes that go self >)
        """
        nodeList = []
        # getting all the nodeName > nodes
        if (smer == "Out" or smer == "Both"):
            for edge in self.edgeList:
                nodeList.append(edge.toNode)
        # getting all the nodeName < nodes
        if (smer == "In" or smer == "Both"):
            for node in graph.nodeList:
                for edge in node.edgeList:
                    if (edge.toNode.nazev == self.nazev):
                        nodeList.append(node)
        # make list items unique
        nodeList = list(set(nodeList))
        if not silent:
            for node in nodeList:
                node.printIsoNode()
        return nodeList 
        
    def surrounding(self, graph, smer="Both", silent=False):
        """
        Function that is to display all the edges that go from, to or both way from the node
        @param graph: graph which the node is part of
        @param smer: way which to search for (in for edges that go > self, out for edges that go self >)
        @param silent: if True, do not print anything, just return the list of neigbours
        """
        edgeList = []
        # getting all the edges from the node > out direction
        if (smer == "Out" or smer == "Both"):
            for edge in self.edgeList:
                edgeList.append(edge)
        if (smer == "In" or smer == "Both"):
            for node in graph.nodeList:
                for edge in node.edgeList:
                    if (edge.toNode.nazev == self.nazev):
                        edgeList.append(edge)
                        
        edgeList = list(set(edgeList))
        for edge in edgeList:
            if smer == "Out" and not silent:
                edge.printIsoEdge(self.nazev)
            if smer == "In" and not silent:
                edge.printIsoEdge(graph.findNodeByEdge(edge).nazev)
            if smer == "Both" and not silent:
                if edge.toNode.nazev == self.nazev:
                    edge.printIsoEdge(graph.findNodeByEdge(edge).nazev)
                else:
                    edge.printIsoEdge(self.nazev)
        return edgeList
            
    def degree(self, graph, smer="Both", silent=False):
        """
        Calculate the size of neigbours of a node
        @param graph: graph which the node is part of
        @param smer: way which to search for (in for edges that go > self, out for edges that go self >)
        @return: degree of the given node        
        """
        size = len(self.surrounding(graph, smer, silent=True))
        if not silent:
            print "#!degree="+str(size)
        return size
            
    def connectsTo(self, node, graph):
        self.found = False
        
        def search(startFromNode, nodeToSearch):
            for edge in startFromNode.edgeList:
                if edge.toNode == nodeToSearch:
                    self.found = True
                else:
                    for child in startFromNode.neighbors(graph, smer="Both", silent=True):
                        search(child, nodeToSearch)
             
        search(self, node)
        return self.found        
    
    def connectsDirectly(self, nodeTo):
        toNodes = []
        for edge in self.edgeList:
            toNodes.append(edge.toNode)
        if nodeTo in toNodes:
            return True
        else:
            return False
        
    def printIsoNode(self, silent=False):
        """
        Print node in requested ISOish like format
        """
        string =  "u " + self.nazev
        if self.hodnota:
            string += " " + str(self.hodnota)
        if self.hodnota == 0:
            string += " "+str(self.hodnota)
        if not silent:
            print string
        return string
    def clone(self):
        """
        clones a new instance of itself with same values
        """
                        