'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
import sys
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
    
        
    def main(self, argv):
        parser = Parser()
        parser.purpose = "self.graph.cycles()"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main(sys.argv)