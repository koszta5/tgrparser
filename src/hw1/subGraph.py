'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
import sys
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
    def parseCommandLineNodes(self, args):
        del args[0]
        return str(args)
    def main(self, argv):
        parser = Parser()
        letterList = self.parseCommandLineNodes(argv)
        parser.purpose = "self.graph.subGraph(\""+letterList+"\")"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main(sys.argv)