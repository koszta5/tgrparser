'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
import sys
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
    def parseDirectionArgs(self,direction):
        if direction == "out":
            return "Out"
        if direction == "in":
            return "In"
        else: 
            return "Both"
        
    def main(self, argv):
        parser = Parser()
        nodeName = argv[1]
        try:
            direction = argv[2]
        except IndexError,e:
            direction = "Both"
        direction = self.parseDirectionArgs(direction)
        parser.purpose = "self.graph.findNodeByName('"+nodeName+"').degree(self.graph,'"+direction+"')"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main(sys.argv)