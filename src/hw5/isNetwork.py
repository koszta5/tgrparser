'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
    def main(self):
        parser = Parser()
        parser.purpose = "self.graph.isNetwork()"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main()