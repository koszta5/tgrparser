'''
Created on Feb 16, 2012

@author: Pavel Kostelnik
'''
import sys
from parserClass import Parser 
class Main:
    def __init__(self):
        pass
    
        
    def main(self, argv):
        parser = Parser()
        sourceName = "S"
        stockName = "T"
        try:
            printType = argv[1]
        except IndexError,e:
            printType = "FLOW"
        
        parser.purpose = "self.graph.edmonds('"+sourceName+"','"+stockName+"','"+printType+"')"
        parser.run()

if __name__ == '__main__':
    app = Main()
    app.main(sys.argv)