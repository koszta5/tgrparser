'''
Created on Apr 3, 2012

@author: Pavel Kostelnik
'''
from src.iQuickSort import iQuickSort
from node import Node
from edge import Edge
import collections

class Util(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    def sortListOfLists(self, listOfLists):
        """
        This function sorts lists of lists
        @param listOfLists: list to sort
        """
        quickSort = iQuickSort()
        listOfListsCopy = list(listOfLists)
        beginList = []
        for subList in listOfLists:
            subList = quickSort.quicksort(subList, 0, len(subList)-1)
        for subList in listOfLists:
            beginList.append(subList[0])
        sortBegin = list(beginList)
        quickSort.quicksort(sortBegin, 0, len(sortBegin)-1)
        for startNode in sortBegin:
            originalPosition = beginList.index(startNode)
            newPosition = sortBegin.index(startNode)
            listOfListsCopy[newPosition]=listOfLists[originalPosition]
        return listOfListsCopy  


    def uniqueListOfLists(self, seq):
        """
        @param seq: list of lists to uniqize
        I took this piece of code from http://www.peterbe.com/plog/uniqifiers-benchmark
        I think it is reallly terribly written and I can only guess what happens on the last line but it works :)
        I adjusted it later A LOT
        """
        
        unique_data = [list(x) for x in set(tuple(x) for x in seq)]
    
        uniqueDataCopy = list(unique_data)
        uniqueDataCopy2 = list(uniqueDataCopy)
        for subList in uniqueDataCopy:
            skip = True
            for i in range(0,len(uniqueDataCopy2)):
                if set(subList) == set(uniqueDataCopy2[i]):
                    if skip:
                        skip = False
                    else:
                        unique_data[i] = None
        unique_data = filter(lambda x: not x == None, unique_data)
        return unique_data
    def findFirstByAlphabet(self, listToSearch):
        """
        returns the positon of a letter first by alphabet in a list of nodes
        @return: index of the node whose letter is frist in alphabet
        """ 
        sortedList = list(listToSearch)
        sorter = iQuickSort()
        sorter.quicksort(sortedList, 0,len(sortedList)-1)
        return listToSearch.index(sortedList[0])
        
    def orderCycle(self, listOfNodes):
        """
        Orders cycle without breaking the continuosity of the cycle
        """
        firstNode = listOfNodes[self.findFirstByAlphabet(listOfNodes)]
        listOfNodes = collections.deque(listOfNodes)
        while firstNode != listOfNodes[0]:
            listOfNodes.rotate(1)
        return listOfNodes
    
    def nodeListToLetterList(self,nodeList):
        """
        Simple convertor to convert node objects to letters
        """
        lettters = []
        for node in nodeList:
            lettters.append(node.nazev)
        return lettters 
    def removeOpositeEdge(self, edge,graph):
        opositeNode = edge.toNode
        currentNode = graph.findNodeByEdge(edge)
        for edge2 in opositeNode.edgeList:
            if edge2.toNode == currentNode:
                opositeNode.edgeList.remove(edge2)
    def parseCyclesAndRemoveShort(self, cycleList):
        for cycle in cycleList:
            if len(cycle) == 2:
                cycleList.remove(cycle)
        return cycleList
    
    def orderListOfEdgesBySize(self, listOfEdges):
        listOfEdges.sort(key=lambda x: x.hodnota, reverse=False)
        return listOfEdges
    def fixDuplicitsInSpanningTree(self, listOfEdges):
        half = len(listOfEdges)/2
        return listOfEdges[:half]
        
    def findTreeWithEdgeFrom(self, edge, listOfTrees):
        for tree in listOfTrees:
            for node in tree:
                if edge in node.edgeList:
                    return tree
    def findTreeWithEdgeTo(self, edge, listOfTrees):
        for tree in listOfTrees:
            for node in tree:
                if edge.toNode == node:
                    return tree 
                
    def findWayTo(self, startNode, endNode):
            currNode = endNode
            backtrackPath = []
            backtrackPath.append(currNode)
            
            pathExists = True
            while currNode != startNode:
                try:
                    backtrackPath.append(currNode.prev.findEdgeByNodeTo(currNode))
                    backtrackPath.append(currNode.prev)
                    currNode = currNode.prev
                except AttributeError,e:
                    print "#!pathNotExists"
                    pathExists = False
                    break
            
            backtrackPath.reverse()

            if (pathExists):
                length = 0   
                for i, obj in enumerate((backtrackPath)):
                    if isinstance(obj, Edge):
                        length = length + obj.hodnota
                print "#!value="+str(length)
                
                for i, obj in enumerate((backtrackPath)):
                    if isinstance(obj, Node):
                        obj.printIsoNode()
                    if isinstance(obj, Edge):
                        obj.printIsoEdge(backtrackPath[i-1])
                        
    def make_list(self, size):
        """create a list of size number of zeros"""
        mylist = []
        for i in range(size):
            mylist.append(0)
        return mylist
    def make_matrix(self, rows, cols):
        """
        create a 2D matrix as a list of rows number of lists
        where the lists are cols in size
        resulting matrix contains zeros
        """
        matrix = []
        for i in range(rows):
            matrix.append(self.make_list(cols))
        return matrix
    
    def create_capacity_matrix(self, graph):
        matrix = []
        for node in graph.nodeList:
            row = []
            for node2 in graph.nodeList:
                if node.connectsDirectly(node2):
                    row.append(int(node.findEdgeByNodeTo(node2).hodnota))
                else:
                    row.append(0)
            matrix.append(row)
        return matrix
    
    def ordinalize_list_of_nodes(self,graph):
        for i,node in enumerate(graph.nodeList):
            node.ord = i
    
    def tuple_ord_path_to_node_list(self, graph, walk_of_tuples):
        string = ""
        edges = []
        first = True
        for fromNode,toNode in walk_of_tuples:
            if first:
                string += graph.findNodeByOrd(fromNode).nazev
                string += ","+graph.findNodeByOrd(toNode).nazev
                first = False
            else:
                string += ","+graph.findNodeByOrd(toNode).nazev
            
            edges.append(graph.findNodeByOrd(fromNode).findEdgeByNodeTo(graph.findNodeByOrd(toNode)))
        return string, edges
    
    def backup_edges_value(self, graph):
        for node in graph.nodeList:
            for edge in node.edgeList:
                edge.orig_hodnota = edge.hodnota
    
    def find_edge_by_nodes_ord_numbers(self, graph, fromNodeOrd, toNodeOrd):
        edge = graph.findNodeByOrd(fromNodeOrd).findEdgeByNodeTo(graph.findNodeByOrd(toNodeOrd))
        return edge
    
    def debug_ord_matrices(self, matrix, capacity_matrix, graph):
        
        
        for i in range(0,len(matrix)):
            for j in range(0,len(matrix)):
                try:
                    print "value is"+str(capacity_matrix[i][j] - matrix[i][j])
                    #print "values is"+str(matrix[i][j])
                    print "from "
                    graph.findNodeByOrd(i).printIsoNode()
                    print "to"
                    graph.findNodeByOrd(j).printIsoNode()
                    edge = self.find_edge_by_nodes_ord_numbers(graph, i, j)
                    edge.printIsoEdge(graph.findNodeByOrd(i))
                    
                except AttributeError,e:
                    print "i"
    
    def compute_new_edges_value_from_matrix(self, graph, residual_matrix, capacity_matrix):
        for i in range(0, len(residual_matrix)):
            for j in range(0, len(residual_matrix)):
                edge = graph.findNodeByOrd(i).findEdgeByNodeTo(graph.findNodeByOrd(j))
                if (residual_matrix[i][j] > 0):
                    if residual_matrix[i][j] == capacity_matrix[i][j]:
                        edge.hodnota = residual_matrix[i][j]
                        edge.printIsoEdge(graph.findNodeByEdge(edge))
                    else:
                        edge.hodnota = 99999999999999999
                else:
                    try:
                        edge.hodnota = 99999999999999999
                    except AttributeError,e:
                        pass
                    #edge.hodnota = capacity_matrix[i][j] - residual_matrix[i][j]
                    #self.debug_ord_matrices(matrix, matrix, graph)
        
                
    
    
        
        
